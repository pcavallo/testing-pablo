package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class NuevoContactoPage {
	private WebDriver driver;
	private By cNombre;
	private By cEmpresa;
	private By cMail;
	private By cTelefono;
	private By guardar;
	public NuevoContactoPage(WebDriver driver) {
		this.driver = driver;
		cNombre = By.id("input_name");
		cEmpresa = By.id("input_company2");
		cMail = By.id("input_mail");
		cTelefono = By.id("input_phone");
		guardar = By.id("wc_customer_form_save");
	}
	
	public void crearContacto(String nombre, String empresa, String email, String telefono) {
		driver.findElement(cNombre).sendKeys(nombre);
		driver.findElement(cEmpresa).sendKeys(empresa);
		driver.findElement(cMail).sendKeys(email);
		driver.findElement(cTelefono).sendKeys(telefono);
		driver.findElement(guardar).click();
	}
	
	//verifica un elemento si existe o no. Si existe va a decir que el campo
	//tiene al menos 1 elemento.
	public boolean paginaEncontrada() {
		return driver.findElements(cMail).size()>0;
	}
	
}
