package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ContactoPage {
	private WebDriver driver;
	private By cartelNombre;
	public ContactoPage(WebDriver driver) {
		this.driver = driver;
		cartelNombre = By.xpath("/html/body/div[1]/div[1]/div[1]/div");
	}
	
	public String textoNombre() {
		return driver.findElement(cartelNombre).getText();
	}
}
