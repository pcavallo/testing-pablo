package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ContactosPage {
	private WebDriver driver;
	private By nuevoContacto;
	public ContactosPage(WebDriver driver) {
		this.driver = driver;
		nuevoContacto = By.id("wc_customer_new");
	}
	
	public void irNuevoContacto() {
		driver.switchTo().frame("wc_customer");
		WebDriverWait wait=new WebDriverWait(driver, 20);
		WebElement e = wait.until(ExpectedConditions.elementToBeClickable(nuevoContacto));
		e.click();
	}
}
