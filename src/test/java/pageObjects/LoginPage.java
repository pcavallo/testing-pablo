package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import extras.Waiter;

public class LoginPage {
	private By user;
	private By password;
	private By loginButton;
	private By cartel;
	private WebDriver driver;
	public LoginPage(WebDriver driver) {
		this.driver = driver;
		user = By.id("txtUsuario");
		password = By.id("txtPass");
		loginButton = By.xpath("/html/body/div/div[1]/div[2]/div/div[1]/div[1]/table/tbody/tr/td/form/table/tbody/tr[3]/td/button");
		cartel = By.xpath("/html/body/div/div[1]/div[2]/div/div[1]/div[5]");
	}
	
	public void login(String usuario, String pass) {
		this.driver.findElement(user).sendKeys(usuario);
		driver.findElement(password).sendKeys(pass);
		driver.findElement(loginButton).click();
		Waiter.waitForPage(10000);
	}
	
	public String cartelMalLogin() {
		return driver.findElement(cartel).getText();
	}
}
