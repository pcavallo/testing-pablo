package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MainPage {
	private WebDriver driver;
	private By contactos;
	public MainPage(WebDriver driver) {
		this.driver = driver;
		contactos = By.xpath("//*[@title='Contactos']");
	}
	
	public void iCcontactos() {
		this.driver.findElement(contactos).click();
	}
	
	public boolean esSitio() {
		return driver.findElements(contactos).size() != 0;

	}
}
