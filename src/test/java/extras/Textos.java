package extras;

import java.util.Random;

public class Textos {
	public static String randomString(int largo,boolean numerico) {
		String candidatos;
		if(numerico == false) {
			candidatos = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		}else {
			candidatos = "1234567890";
		}
		StringBuilder sb = new StringBuilder();
	    Random random = new Random();
	    for (int i = 0; i < largo; i++) {
	        sb.append(candidatos.charAt(random.nextInt(candidatos.length())));
	    }

	    return sb.toString();
	}
}
