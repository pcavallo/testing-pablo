package base;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import extras.DriverManipulator;
import pageObjects.LoginPage;

public class Base {
	protected WebDriver driver;
	private DriverManipulator dm;
	
	public Base() {
		dm = new DriverManipulator();
	}
	@BeforeMethod
	public void beforeMethod() {
		driver = dm.ComenzarDriver();
		driver.navigate().to("https://qa.wcentrix.com/app/#/customers");
		LoginPage lp = new LoginPage(driver);
		lp.login("wcx.admin.webcentrixtesting", ";.y!le:)||bv}$2");
	}
	
	@AfterMethod
	public void afterMethod() {
		dm.CerrarDriver();
	}
}
