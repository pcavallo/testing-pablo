package base;

import org.testng.Assert;
import org.testng.annotations.Test;
import pageObjects.ContactoPage;
import pageObjects.ContactosPage;
import pageObjects.LoginPage;
import pageObjects.MainPage;
import pageObjects.NuevoContactoPage;
import extras.Textos;

public class TestContactos extends Base {
	@Test
	public void NuevoContactoTest() {
		MainPage mp = new MainPage(driver);
		ContactosPage cp = new ContactosPage(driver);
		NuevoContactoPage ncp = new NuevoContactoPage(driver);
		ContactoPage cop = new ContactoPage(driver);
		mp.iCcontactos();
		cp.irNuevoContacto();
		String nombre = Textos.randomString(5,false);
		String empresa = "Draculinio";
		String email = Textos.randomString(5, false) + "@" + Textos.randomString(5, false)+".com";
		String telefono = Textos.randomString(10, true);
		ncp.crearContacto(nombre, empresa, email, telefono);
		Assert.assertEquals(nombre, cop.textoNombre());
	}
	
	@Test
	public void ContactoVacioTest() {
		MainPage mp = new MainPage(driver);
		ContactosPage cp = new ContactosPage(driver);
		NuevoContactoPage ncp = new NuevoContactoPage(driver);
		ContactoPage cop = new ContactoPage(driver);
		mp.iCcontactos();
		cp.irNuevoContacto();
		ncp.crearContacto("", "", "", "");
		Assert.assertTrue(ncp.paginaEncontrada());
	}
	
	@Test
	public void ContactoSinNombreTest() {
		MainPage mp = new MainPage(driver);
		ContactosPage cp = new ContactosPage(driver);
		NuevoContactoPage ncp = new NuevoContactoPage(driver);
		ContactoPage cop = new ContactoPage(driver);
		mp.iCcontactos();
		cp.irNuevoContacto();
		String email = Textos.randomString(5, false) + "@" + Textos.randomString(5, false)+".com";
		String telefono = Textos.randomString(10, true);
		ncp.crearContacto("", "Draculinio", email, telefono);
		Assert.assertTrue(ncp.paginaEncontrada());
	}
	
	
}
