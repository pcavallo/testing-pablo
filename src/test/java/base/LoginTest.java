package base;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import extras.DriverManipulator;
import pageObjects.LoginPage;
import pageObjects.MainPage;

public class LoginTest{
	private WebDriver driver;
	@BeforeMethod
	public void beforeMethod() {
		DriverManipulator dm = new DriverManipulator();
		driver = dm.ComenzarDriver();
		driver.navigate().to("https://qa.wcentrix.com/app/#/customers");
	}
	@Test
	public void loginCorrectoTest() {
		LoginPage lp = new LoginPage(driver);
		MainPage mp = new MainPage(driver);
		lp.login("wcx.admin.webcentrixtesting", ";.y!le:)||bv}$2");
		Assert.assertTrue(mp.esSitio());
	}
	
	@Test
	public void loginIncorrectoTest() {
		LoginPage lp = new LoginPage(driver);
		lp.login("wcx.admin.webcentrixtesting", "incorrecto");
		Assert.assertEquals("Error de usuario y/o contraseņa", lp.cartelMalLogin());
	}
}
